" tabs & spaces settings
set tabstop=4  " number of spaces per tab for visual
set shiftwidth=4  
set softtabstop=4  " number of spaces per tab for editing
set expandtab  " tabs to spaces
set autoindent

set number  " enumerate lines
set cursorline " cursor below current line
set showmatch  " highlight matching brackets

set wildmenu  " visual autocomplete for cmd menu
set lazyredraw  " redraw only necessary things

set incsearch  " search as characters are typed
set hlsearch  " highlight search results
" pressing ", " (coma and space) types :nohlsearch (disable search highlight)
nnoremap ,<space> :nohlsearch<CR>

" folding
set foldenable  " enable folding
set foldmethod=syntax " type of folding
set foldlevelstart=91  " 0 all folds closed, 99 all open
" use "space" as za to open/close folds
nnoremap <space> za

" move
nnoremap j gj
nnoremap k gk

" highlight last inserted text
nnoremap gV `[v`]
"
" python
let python_highlight_all = 1

nnoremap <C-n> :NERDTreeToggle<CR>
" launch Gundo tab
nnoremap ,u :GundoToggle<CR>

call plug#begin('~/.vim/plugged')

Plug 'sjl/gundo.vim'
Plug 'preservim/nerdtree'
" TODO: vim complains about post-update hook
"Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'cespare/vim-toml'

call plug#end()
