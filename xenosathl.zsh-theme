typeset -F3 SECONDS  # cast shell variable `SECONDS` to float (to third digit after decimal point)

function print_branch() {
    local my_branch=$1
    git diff-index --quiet HEAD 2>/dev/null
    if [[ $? -eq 0 ]]; then
        echo "[%F{green}$my_branch%f]"
    else
        echo "[%F{red}$my_branch%f]"
    fi
}


function get_branch() {
    if [[ $(git rev-parse --is-inside-work-tree 2>/dev/null) == "true" ]]; then  # in git directory
        local my_branch=$(git symbolic-ref --short -q HEAD 2>/dev/null)
        if [[ $my_branch == '' ]]; then
             print_branch $(git log -1 --pretty=%h)  # detached branch
        else
             print_branch $my_branch
        fi
    fi
}


precmd() {
    if [ $t2 ]; then
        t1=$SECONDS
        elapsed=$(echo $t1 - $t2 | bc)
        unset t2
    else
        elapsed=-1  # no command was executed
    fi
}

preexec() { t2=$SECONDS }

function elapsed_time() {
    if [[ $elapsed -ne -1 ]]; then
        echo in $elapsed s
    fi
}

NLINE=$'\n'
UNSET_STYLES='%f%k%b%u'
PROMPT='$UNSET_STYLES%B%K{black}[%*] %F{yellow}$USER@$HOST%f %F{blue}%~%f $(get_branch)%E %b%K{black}$(elapsed_time)$UNSET_STYLES${NLINE}-> '

# $UNSET_STYLES - prevents application style affect prompt style and vice versa
# [%*] - show time in format [HH:MM:SS], %* is zsh equivalent of `date +'%T'`
# %~ - zsh equivalent of pwd with wrapping /user/home to `~`
# %E - zsh feature; ensures all(?) visual styles will be rendered to end of line (I think so)
# rest (%b, %k, %f, %u; lower- and upppercase) - setting/unsetting zsh visual features
